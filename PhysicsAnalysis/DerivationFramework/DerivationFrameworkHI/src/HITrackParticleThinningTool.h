/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// HITrackParticleThinningTool.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef DERIVATIONFRAMEWORK_HITRACKPARTICLETHINNINGTOOL_H
#define DERIVATIONFRAMEWORK_HITRACKPARTICLETHINNINGTOOL_H


#include<string>
#include<atomic>

// Gaudi & Athena basics
#include "AthenaBaseComps/AthAlgTool.h"
#include "GaudiKernel/ServiceHandle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "StoreGate/ThinningHandleKey.h"

// DerivationFramework includes
#include "DerivationFrameworkInterfaces/IThinningTool.h"
#include "AsgTools/ToolHandle.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"


class IThinningSvc;

namespace DerivationFramework {

  class HITrackParticleThinningTool : public extends<AthAlgTool, IThinningTool> {
    
  public: 
    // Constructor with parameters
    HITrackParticleThinningTool( const std::string& t, const std::string& n, const IInterface* p );
    
    // Destructor
    ~HITrackParticleThinningTool();
    
    // Athena algtool's Hooks
    StatusCode initialize() override;
    StatusCode finalize() override;
    
    // Check that the current event passes this filter 
    virtual StatusCode doThinning() const override;
 
  private:
    StringProperty m_streamName
    { this, "StreamName", "", "Name of the stream being thinned" };
    SG::ThinningHandleKey<xAOD::TrackParticleContainer> m_inDetSGKey
    { this, "InDetTrackParticlesKey", "InDetTrackParticles", "" };

    std::string m_vertex_key;
    std::string m_vertex_scheme;
    ToolHandle< InDet::IInDetTrackSelectionTool > m_trkSelTool; //!< track selection tool which can be optionally used for N_trk and sum pt cuts
    
    mutable std::atomic<unsigned int> m_ntot;
    mutable std::atomic<unsigned int> m_npass;
  }; 
  
}

#endif

