#  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
## Print the output by DumpMC
from TruthIO.TruthIOConf import DumpMC
topAlg += DumpMC()
