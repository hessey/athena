#include "../TrigHIEventShapeHypoAlg.h"
#include "../TrigHIFwdGapHypoTool.h"
#include "../TrigHIUCCHypoTool.h"

DECLARE_COMPONENT( TrigHIEventShapeHypoAlg )
DECLARE_COMPONENT( TrigHIFwdGapHypoTool )
DECLARE_COMPONENT( TrigHIUCCHypoTool )
